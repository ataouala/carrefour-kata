Feature: Get available time slots

  Scenario: Get available time slots for a specific delivery method
    When we get "/api/v1/timeslots/available?deliveryMethod=DRIVE"
    Then we received a status OK and only:
    """
    [
  {
    "id": 1,
    "deliveryMethodId": "DRIVE",
    "startTime": [
      9,
      0
    ],
    "endTime": [
      10,
      0
    ]
  }
]
    """
