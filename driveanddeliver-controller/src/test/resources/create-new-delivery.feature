Feature: Create a new delivery

  Scenario: Create a new delivery successfully
    When we post on "/api/v1/deliveries":
    """
      {
  "deliveryMethodId": "DRIVE",
  "deliveryDate": "2024-06-17",
  "timeSlotId": 1
}
      """
    Then we received:
      """
      {
  "deliveryMethodId": "DRIVE",
  "deliveryDate": [
    2024,
    6,
    17
  ]
}
      """

  Scenario: Attempt to create a delivery with an invalid time slot
    When we post on "/api/v1/deliveries":
    """
      {
  "deliveryMethodId": "DRIVE",
  "deliveryDate": "2024-06-17",
  "timeSlotId": 11
}
      """
    Then we received a status BAD_REQUEST
