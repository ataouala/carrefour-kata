Feature: Get available delivery methods

  Scenario: Get available delivery methods
    When we get "/api/v1/delivery-methods"
    Then we received a status OK and only:
    """
    [
  {
    "id": "DRIVE",
    "method": "DRIVE"
  },
  {
    "id": "DELIVERY",
    "method": "DELIVERY"
  },
  {
    "id": "DELIVERY_TODAY",
    "method": "DELIVERY_TODAY"
  },
  {
    "id": "DELIVERY_ASAP",
    "method": "DELIVERY_ASAP"
  }
]
    """
