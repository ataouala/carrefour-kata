package com.carrefour.driveanddeliver;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;

@RequiredArgsConstructor
@RestController
@RequestMapping("/api/v1/delivery-methods")
@Tag(name = "Delivery Method", description = "API to manage delivery methods")
public class DeliveryMethodController {

    private final DeliveryMethodService deliveryMethodService;


    @Operation(summary = "Get all delivery methods")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Successful operation")
    })
    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public Flux<DeliveryMethodDTO> getAllDeliveryMethods() {
        return deliveryMethodService.getAllDeliveryMethods();
    }
}
