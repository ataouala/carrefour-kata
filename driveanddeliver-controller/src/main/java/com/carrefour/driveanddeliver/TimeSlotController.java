package com.carrefour.driveanddeliver;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;

@Tag(name = "Time Slot", description = "API to manage time slots")
@RequiredArgsConstructor
@RestController
@RequestMapping("/api/v1/timeslots")
public class TimeSlotController {

    private final TimeSlotService timeSlotService;

    @Operation(summary = "Get available time slots")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Successful operation",
                    content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE, array = @ArraySchema(schema = @Schema(implementation = TimeSlotDTO.class))))
    })
    @GetMapping("/available")
    public Flux<TimeSlotDTO> getAvailableTimeSlots(@RequestParam String deliveryMethod) {
        return timeSlotService.getAvailableTimeSlots(deliveryMethod);
    }
}
