package com.carrefour.driveanddeliver;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Info;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.reactive.config.EnableWebFlux;

@SpringBootApplication
@EnableWebFlux
@OpenAPIDefinition(
		info = @Info(
				title = "Delivery Management System API",
				description = "API for managing delivery information.",
				version = "1.0"))
public class DriveanddeliverApplication {


    public static void main(String[] args) {
		SpringApplication.run(DriveanddeliverApplication.class, args);
	}

}
