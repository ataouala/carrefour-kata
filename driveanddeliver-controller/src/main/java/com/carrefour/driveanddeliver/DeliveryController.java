package com.carrefour.driveanddeliver;

import com.carrefour.driveanddeliver.exception.TimeSlotBookingException;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;
import reactor.core.publisher.Mono;

@RequiredArgsConstructor
@RestController
@Tag(name = "Delivery", description = "API to manage deliveries")
@RequestMapping(value = "/api/v1/deliveries", produces = MediaType.APPLICATION_JSON_VALUE)
public class DeliveryController {

    private final DeliveryService deliveryService;

    @Operation(summary = "Create a new delivery")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Delivery created",
                    content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = DeliveryDTO.class))),
            @ApiResponse(responseCode = "400", description = "Invalid input", content = @Content)
    })
    @PostMapping
    public Mono<DeliveryDTO> createDelivery(@RequestBody CreateDeliveryDTO createDeliveryDTO) {
        return deliveryService.createDelivery(createDeliveryDTO)
                .onErrorMap(e -> {
                    if (e instanceof TimeSlotBookingException) {
                        throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage(), e);
                    } else {
                        throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Failed to create delivery", e);
                    }
                });
    }

}
