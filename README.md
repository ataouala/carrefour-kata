# Drive and Deliver Service
[![Java][Java-badge]][Java-url]
[![Spring Boot][SpringBoot-badge]][SpringBoot-url]
[![Maven][Maven-badge]][Maven-url]

[Java-badge]: https://img.shields.io/badge/Java-21-orange
[Java-url]: https://www.java.com/

[SpringBoot-badge]: https://img.shields.io/badge/Spring%20Boot-3.3.0-brightgreen
[SpringBoot-url]: https://spring.io/projects/spring-boot

[Maven-badge]: https://img.shields.io/badge/Maven-3.8.1-yellow
[Maven-url]: https://maven.apache.org/


This project implements a delivery management system using Spring Boot, WebFlux, and Cucumber. The main objective is to allow customers to choose their delivery method and schedule their delivery time slot.

## Technologies Used

- **Java 21**: Programming language version.
- **Spring Boot 3.x.x**: Framework for building Java applications.
- **Spring WebFlux**: Reactive programming support in Spring.
- **Cucumber**: Framework for writing integration tests in BDD (Behavior-Driven Development) style.
- **Docker**: Containerization platform.

## Project Structure

```bash
driveanddeliver
├── .idea
├── .mvn
├── driveanddeliver-controller
├── driveanddeliver-dao
├── driveanddeliver-dto
├── driveanddeliver-entity
├── driveanddeliver-service
├── .gitignore
├── .gitlab-ci.yml
├── Dockerfile
├── HELP.md
├── mvnw
├── mvnw.cmd
├── pom.xml
├── README.md
├── DeliveryController.java
├── DriveanddeliverApplication.java
└── TimeSlotController.java
````
## How to Start the Application

### Prerequisites

- Java 21
- Maven 3.8.1 or later
- Docker (optional, for containerized deployment)

### Running Locally

1. **Clone the repository:**

    ```bash
    git clone https://github.com/your-repository/driveanddeliver.git
    cd driveanddeliver
    ```

2. **Build the project:**

    ```bash
    ./mvnw clean install
    ```

3. **Run the application:**

    ```bash
    ./mvnw spring-boot:run
    ```

### Running with Docker

1. **Build the Docker image:**

    ```bash
    docker build -t driveanddeliver:latest .
    ```

2. **Run the Docker container:**

    ```bash
    docker run -p 8080:8080 driveanddeliver:latest
    ```

### Running Tests

1. **Run unit tests:**

    ```bash
    ./mvnw test
    ```

2. **Run integration tests with Cucumber:**

    ```bash
    ./mvnw verify
    ```
