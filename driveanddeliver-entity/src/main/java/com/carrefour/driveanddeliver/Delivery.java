package com.carrefour.driveanddeliver;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Table;

import java.time.LocalDate;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Table("deliveries")
public class Delivery {

    @Id
    private Long id;
    private String deliveryMethodId;
    private LocalDate deliveryDate;
    private Long timeSlotId;
    private Integer userId;

}
