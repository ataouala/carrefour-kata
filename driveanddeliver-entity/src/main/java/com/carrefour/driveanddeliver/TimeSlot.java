package com.carrefour.driveanddeliver;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Table;

import java.time.LocalTime;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Table("time_slots")
public class TimeSlot {

    @Id
    private Long id;
    private String deliveryMethodId;
    private LocalTime startTime;
    private LocalTime endTime;
    private int availableSlots;
}
