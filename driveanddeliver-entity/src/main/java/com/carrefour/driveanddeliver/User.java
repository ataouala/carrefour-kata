package com.carrefour.driveanddeliver;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Column;
import org.springframework.data.relational.core.mapping.Table;

@NoArgsConstructor
@Getter
@Table(name = "users")
public class User {

    @Id
    Integer id;

    @Column("first_name")
    String firstName;

    @Column("last_name")
    String lastName;
}
