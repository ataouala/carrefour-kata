package com.carrefour.driveanddeliver;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Table;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Table("delivery_methods")
public class DeliveryMethod {

    @Id
    private String id;

    private String method;
}
