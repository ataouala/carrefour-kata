package com.carrefour.driveanddeliver;

import com.carrefour.driveanddeliver.exception.ErrorCode;
import com.carrefour.driveanddeliver.exception.TimeSlotBookingException;
import com.carrefour.driveanddeliver.impl.DeliveryServiceImpl;
import com.carrefour.driveanddeliver.mapper.DeliveryMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class DeliveryServiceTest {

    @Mock
    private DeliveryRepository deliveryRepository;

    @Mock
    private DeliveryMapper deliveryMapper;

    @Mock
    private TimeSlotService timeSlotService;

    @InjectMocks
    private DeliveryServiceImpl deliveryService;

    private CreateDeliveryDTO createDeliveryDTO;
    private Delivery delivery;
    private DeliveryDTO deliveryDTO;
    private TimeSlotDTO timeSlotDTO;

    @BeforeEach
    void setUp() {

        createDeliveryDTO = new CreateDeliveryDTO();
        createDeliveryDTO.setTimeSlotId(1L);
        createDeliveryDTO.setDeliveryMethodId("DRIVE");

        delivery = new Delivery();
        delivery.setId(1L);
        delivery.setTimeSlotId(1L);

        deliveryDTO = new DeliveryDTO();

        timeSlotDTO = new TimeSlotDTO();
        timeSlotDTO.setId(1L);
    }

    @Test
    void testCreateDelivery_Successful() {
        when(deliveryMapper.toDelivery(any(CreateDeliveryDTO.class))).thenReturn(delivery);
        when(timeSlotService.bookTimeSlot(anyLong(), anyString())).thenReturn(Mono.just(timeSlotDTO));
        when(deliveryRepository.save(any(Delivery.class))).thenReturn(Mono.just(delivery));
        when(deliveryMapper.toDeliveryDTO(any(Delivery.class))).thenReturn(deliveryDTO);

        Mono<DeliveryDTO> result = deliveryService.createDelivery(createDeliveryDTO);

        StepVerifier.create(result)
                .expectNext(deliveryDTO)
                .verifyComplete();

        verify(deliveryMapper).toDelivery(createDeliveryDTO);
        verify(timeSlotService).bookTimeSlot(1L, "DRIVE");
        verify(deliveryRepository).save(delivery);
        verify(deliveryMapper).toDeliveryDTO(delivery);
    }

    @Test
    void testCreateDelivery_TimeSlotNotAvailable() {
        when(timeSlotService.bookTimeSlot(anyLong(), anyString())).thenReturn(Mono.empty());

        Mono<DeliveryDTO> result = deliveryService.createDelivery(createDeliveryDTO);

        StepVerifier.create(result)
                .expectErrorMatches(throwable -> throwable instanceof TimeSlotBookingException &&
                        ((TimeSlotBookingException) throwable).getErrorCode() == ErrorCode.TIME_SLOT_NOT_AVAILABLE)
                .verify();

        verify(timeSlotService).bookTimeSlot(1L, "DRIVE");
        verify(deliveryRepository, never()).save(any(Delivery.class));
        verify(deliveryMapper, never()).toDeliveryDTO(any(Delivery.class));
    }

    @Test
    void testCreateDelivery_SaveFailure() {
        when(deliveryMapper.toDelivery(any(CreateDeliveryDTO.class))).thenReturn(delivery);
        when(timeSlotService.bookTimeSlot(anyLong(), anyString())).thenReturn(Mono.just(timeSlotDTO));
        when(deliveryRepository.save(any(Delivery.class))).thenReturn(Mono.error(new RuntimeException("Save failed")));

        Mono<DeliveryDTO> result = deliveryService.createDelivery(createDeliveryDTO);

        StepVerifier.create(result)
                .expectError(RuntimeException.class)
                .verify();

        verify(deliveryMapper).toDelivery(createDeliveryDTO);
        verify(timeSlotService).bookTimeSlot(1L, "DRIVE");
        verify(deliveryRepository).save(delivery);
        verify(deliveryMapper, never()).toDeliveryDTO(any(Delivery.class));
    }
}
