package com.carrefour.driveanddeliver;

import com.carrefour.driveanddeliver.impl.DeliveryMethodServiceImpl;
import com.carrefour.driveanddeliver.mapper.DeliveryMethodMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import reactor.core.publisher.Flux;
import reactor.test.StepVerifier;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class DeliveryMethodServiceTest {

    @Mock
    private DeliveryMethodRepository deliveryMethodRepository;

    @Mock
    private DeliveryMethodMapper deliveryMethodMapper;

    @InjectMocks
    private DeliveryMethodServiceImpl deliveryMethodService;

    private DeliveryMethod deliveryMethod;
    private DeliveryMethodDTO deliveryMethodDTO;

    @BeforeEach
    void setUp() {

        deliveryMethod = new DeliveryMethod();
        deliveryMethod.setId("DRIVE");

        deliveryMethodDTO = new DeliveryMethodDTO();
        deliveryMethodDTO.setId("DRIVE");
    }

    @Test
    void testGetAllDeliveryMethods_Successful() {
        when(deliveryMethodRepository.findAll()).thenReturn(Flux.just(deliveryMethod));
        when(deliveryMethodMapper.toDto(any(DeliveryMethod.class))).thenReturn(deliveryMethodDTO);

        Flux<DeliveryMethodDTO> result = deliveryMethodService.getAllDeliveryMethods();

        StepVerifier.create(result)
                .expectNext(deliveryMethodDTO)
                .verifyComplete();

        verify(deliveryMethodRepository).findAll();
        verify(deliveryMethodMapper).toDto(deliveryMethod);
    }

    @Test
    void testGetAllDeliveryMethods_Empty() {
        when(deliveryMethodRepository.findAll()).thenReturn(Flux.empty());

        Flux<DeliveryMethodDTO> result = deliveryMethodService.getAllDeliveryMethods();

        StepVerifier.create(result)
                .verifyComplete();

        verify(deliveryMethodRepository).findAll();
        verify(deliveryMethodMapper, never()).toDto(any(DeliveryMethod.class));
    }
}
