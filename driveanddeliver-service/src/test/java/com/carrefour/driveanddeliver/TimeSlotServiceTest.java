package com.carrefour.driveanddeliver;

import com.carrefour.driveanddeliver.exception.ErrorCode;
import com.carrefour.driveanddeliver.exception.TimeSlotBookingException;
import com.carrefour.driveanddeliver.impl.TimeSlotServiceImpl;
import com.carrefour.driveanddeliver.mapper.TimeSlotMapper;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

import java.time.LocalTime;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.anyInt;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class TimeSlotServiceTest {

    @Mock
    private TimeSlotRepository timeSlotRepository;
    @Mock
    private TimeSlotMapper timeSlotMapper;


    @InjectMocks
    private TimeSlotServiceImpl timeSlotService;


    private List<TimeSlot> getMockTimeSlots() {
        return Arrays.asList(
                new TimeSlot(1L, "DRIVE", LocalTime.parse("09:00"), LocalTime.parse("10:00"), 5),
                new TimeSlot(2L, "DELIVERY", LocalTime.parse("11:00"), LocalTime.parse("12:00"), 3),
                new TimeSlot(3L, "DELIVERY_TODAY", LocalTime.parse("13:00"), LocalTime.parse("14:00"), 4)
        );
    }

    @Test
    void testGetAvailableTimeSlots_Successful() {
        when(timeSlotRepository.findByDeliveryMethodIdAndAvailableSlotsGreaterThan(anyString(), anyInt()))
                .thenReturn(Flux.fromIterable(getMockTimeSlots()));

        when(timeSlotMapper.toTimeSlotDTO(any())).thenAnswer(invocation -> {
            TimeSlot timeSlot = invocation.getArgument(0);
            return new TimeSlotDTO(timeSlot.getId(), timeSlot.getDeliveryMethodId(),
                    timeSlot.getStartTime(), timeSlot.getEndTime());
        });

        Flux<TimeSlotDTO> result = timeSlotService.getAvailableTimeSlots("DRIVE");

        assertEquals(3, result.count().block());
    }

    @Test
    void testGetAvailableTimeSlots_NoAvailableSlots() {
        when(timeSlotRepository.findByDeliveryMethodIdAndAvailableSlotsGreaterThan(anyString(), anyInt()))
                .thenReturn(Flux.fromIterable(Arrays.asList()));
        Flux<TimeSlotDTO> result = timeSlotService.getAvailableTimeSlots("UNKNOWN");

        assertEquals(0, result.count().block());
        // Add more assertions as needed
    }

    @Test
    void testBookTimeSlot_SuccessfulBooking() {
        // Mock behavior for timeSlotRepository
        when(timeSlotRepository.findByIdAndDeliveryMethodId(anyLong(), anyString()))
                .thenReturn(Mono.just(new TimeSlot(1L, "DRIVE", LocalTime.parse("09:00"), LocalTime.parse("10:00"), 1)));

        when(timeSlotRepository.save(any()))
                .thenAnswer(invocation -> Mono.just(invocation.getArgument(0)));
        // Mock behavior for timeSlotMapper
        when(timeSlotMapper.toTimeSlotDTO(any())).thenAnswer(invocation -> {
            TimeSlot timeSlot = invocation.getArgument(0);
            return new TimeSlotDTO(timeSlot.getId(), timeSlot.getDeliveryMethodId(),
                    timeSlot.getStartTime(), timeSlot.getEndTime());
        });

        Mono<TimeSlotDTO> result = timeSlotService.bookTimeSlot(1L, "DRIVE");

        assertEquals(1L, result.block().getId());
        // Add more assertions as needed
    }

    @Test
    void testBookTimeSlot_TimeSlotAlreadyBooked() {
        when(timeSlotRepository.findByIdAndDeliveryMethodId(anyLong(), anyString()))
                .thenReturn(Mono.just(new TimeSlot(1L, "DRIVE", LocalTime.parse("09:00"), LocalTime.parse("10:00"), 0)));

        Mono<TimeSlotDTO> result = timeSlotService.bookTimeSlot(1L, "DRIVE");

        result.onErrorResume(TimeSlotBookingException.class, ex -> {
            assertEquals(ErrorCode.TIME_SLOT_ALREADY_BOOKED, ex.getErrorCode());
            return Mono.empty();
        });
        // Add more assertions as needed
    }

    @Test
    void testBookTimeSlot_InvalidTimeSlotId() {
        when(timeSlotRepository.findByIdAndDeliveryMethodId(anyLong(), anyString()))
                .thenReturn(Mono.empty());
        StepVerifier.create(timeSlotService.bookTimeSlot(100L, "DRIVE"))
                .verifyComplete();

    }
}
