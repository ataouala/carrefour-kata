package com.carrefour.driveanddeliver.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class TimeSlotNotFoundException extends RuntimeException {

    public TimeSlotNotFoundException(ErrorCode errorCode) {
        super(errorCode.name());
    }

    public TimeSlotNotFoundException(ErrorCode errorCode, Throwable cause) {
        super(errorCode.name(), cause);
    }
}
