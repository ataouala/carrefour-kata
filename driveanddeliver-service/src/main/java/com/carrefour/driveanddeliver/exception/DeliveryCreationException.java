package com.carrefour.driveanddeliver.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST)
public class DeliveryCreationException extends RuntimeException {

    public DeliveryCreationException(ErrorCode errorCode) {
        super(errorCode.name());
    }

    public DeliveryCreationException(ErrorCode errorCode, Throwable cause) {
        super(errorCode.name(), cause);
    }
}
