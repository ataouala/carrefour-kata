package com.carrefour.driveanddeliver.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST)
public class NoAvailableTimeSlotException extends RuntimeException {
    public NoAvailableTimeSlotException(ErrorCode errorCode) {
        super(errorCode.name());
    }
}
