package com.carrefour.driveanddeliver.exception;

public enum ErrorCode {
    TIME_SLOT_ALREADY_BOOKED("Time slot already booked"),
    TIMESLOT_NOT_FOUND("Time slot not found"),
    TIMESLOT_BOOKING_ERROR("Error occurred while booking time slot"),
    TIME_SLOT_NOT_AVAILABLE("Time slot is not available for booking");

    private final String message;

    ErrorCode(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }
}

