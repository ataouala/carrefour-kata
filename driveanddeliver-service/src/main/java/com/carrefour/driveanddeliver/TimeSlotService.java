package com.carrefour.driveanddeliver;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface TimeSlotService {

    Flux<TimeSlotDTO> getAvailableTimeSlots(String deliveryMethodId);

    Mono<TimeSlotDTO> bookTimeSlot(Long id, String deliveryMethodId);
}
