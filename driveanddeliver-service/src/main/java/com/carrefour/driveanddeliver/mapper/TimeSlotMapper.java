package com.carrefour.driveanddeliver.mapper;

import com.carrefour.driveanddeliver.CreateTimeSlotDTO;
import com.carrefour.driveanddeliver.TimeSlot;
import com.carrefour.driveanddeliver.TimeSlotDTO;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface TimeSlotMapper {

    TimeSlotDTO toTimeSlotDTO(TimeSlot timeSlot);

    TimeSlot toTimeSlot(CreateTimeSlotDTO createTimeSlotDTO);
}
