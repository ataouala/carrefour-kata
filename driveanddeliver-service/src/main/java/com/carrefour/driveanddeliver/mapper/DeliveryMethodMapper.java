package com.carrefour.driveanddeliver.mapper;

import com.carrefour.driveanddeliver.DeliveryMethod;
import com.carrefour.driveanddeliver.DeliveryMethodDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface DeliveryMethodMapper {


    @Mapping(source = "id", target = "id")
    @Mapping(source = "method", target = "method")
    DeliveryMethodDTO toDto(DeliveryMethod deliveryMethod);

    @Mapping(source = "id", target = "id")
    @Mapping(source = "method", target = "method")
    DeliveryMethod toEntity(DeliveryMethodDTO deliveryMethodDTO);
}
