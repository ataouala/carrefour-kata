package com.carrefour.driveanddeliver.mapper;

import com.carrefour.driveanddeliver.CreateDeliveryDTO;
import com.carrefour.driveanddeliver.Delivery;
import com.carrefour.driveanddeliver.DeliveryDTO;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface DeliveryMapper {
    DeliveryDTO toDeliveryDTO(Delivery delivery);
    Delivery toDelivery(CreateDeliveryDTO createDeliveryDTO);
}
