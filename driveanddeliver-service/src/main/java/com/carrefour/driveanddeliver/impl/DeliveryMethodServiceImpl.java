package com.carrefour.driveanddeliver.impl;

import com.carrefour.driveanddeliver.DeliveryMethodDTO;
import com.carrefour.driveanddeliver.DeliveryMethodRepository;
import com.carrefour.driveanddeliver.DeliveryMethodService;
import com.carrefour.driveanddeliver.mapper.DeliveryMethodMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;

@RequiredArgsConstructor
@Service
public class DeliveryMethodServiceImpl implements DeliveryMethodService {

    private final DeliveryMethodRepository deliveryMethodRepository;
    private final DeliveryMethodMapper deliveryMethodMapper;



    @Override
    public Flux<DeliveryMethodDTO> getAllDeliveryMethods() {
        return deliveryMethodRepository.findAll()
                .map(deliveryMethodMapper::toDto);
    }
}
