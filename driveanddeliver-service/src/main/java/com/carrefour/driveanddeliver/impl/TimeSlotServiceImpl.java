package com.carrefour.driveanddeliver.impl;

import com.carrefour.driveanddeliver.TimeSlotDTO;
import com.carrefour.driveanddeliver.TimeSlotRepository;
import com.carrefour.driveanddeliver.TimeSlotService;
import com.carrefour.driveanddeliver.exception.ErrorCode;
import com.carrefour.driveanddeliver.exception.TimeSlotBookingException;
import com.carrefour.driveanddeliver.mapper.TimeSlotMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RequiredArgsConstructor
@Service
public class TimeSlotServiceImpl implements TimeSlotService {

    private final TimeSlotRepository timeSlotRepository;
    private final TimeSlotMapper timeSlotMapper;

    @Override
    public Flux<TimeSlotDTO> getAvailableTimeSlots(String deliveryMethodId) {
        return timeSlotRepository.findByDeliveryMethodIdAndAvailableSlotsGreaterThan(deliveryMethodId, 0)
                .map(timeSlotMapper::toTimeSlotDTO);
    }

    @Override
    public Mono<TimeSlotDTO> bookTimeSlot(Long id, String deliveryMethodId) {
        return timeSlotRepository.findByIdAndDeliveryMethodId(id, deliveryMethodId)
                .flatMap(timeSlot -> {
                    if (timeSlot.getAvailableSlots() == 0) {
                        return Mono.error(new TimeSlotBookingException(ErrorCode.TIME_SLOT_ALREADY_BOOKED));
                    }
                    timeSlot.setAvailableSlots(timeSlot.getAvailableSlots() - 1);
                    return timeSlotRepository.save(timeSlot);
                }).map(timeSlotMapper::toTimeSlotDTO);
    }
}
