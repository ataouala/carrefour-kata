package com.carrefour.driveanddeliver.impl;

import com.carrefour.driveanddeliver.CreateDeliveryDTO;
import com.carrefour.driveanddeliver.Delivery;
import com.carrefour.driveanddeliver.DeliveryDTO;
import com.carrefour.driveanddeliver.DeliveryRepository;
import com.carrefour.driveanddeliver.DeliveryService;
import com.carrefour.driveanddeliver.TimeSlotService;
import com.carrefour.driveanddeliver.exception.ErrorCode;
import com.carrefour.driveanddeliver.exception.TimeSlotBookingException;
import com.carrefour.driveanddeliver.mapper.DeliveryMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import reactor.core.publisher.Mono;

@RequiredArgsConstructor
@Service
public class DeliveryServiceImpl implements DeliveryService {

    private final DeliveryRepository deliveryRepository;
    private final DeliveryMapper deliveryMapper;
    private final TimeSlotService timeSlotService;

    @Transactional
    @Override
    public Mono<DeliveryDTO> createDelivery(CreateDeliveryDTO createDeliveryDTO) {
        // Create Delivery entity from DTO
        Delivery delivery = deliveryMapper.toDelivery(createDeliveryDTO);

        // Attempt to book the associated TimeSlot
        return timeSlotService.bookTimeSlot(createDeliveryDTO.getTimeSlotId(), createDeliveryDTO.getDeliveryMethodId())
                .flatMap(bookedTimeSlot -> {

                    // TODO: Retrieve the authenticated user from Spring Security context & Set the user ID or LDAP identifier to the delivery entity
                    // delivery.setUserId(loggedInUser);

                    delivery.setTimeSlotId(bookedTimeSlot.getId()); // Set the booked time slot ID
                    return deliveryRepository.save(delivery)
                            .map(deliveryMapper::toDeliveryDTO);
                })
                .switchIfEmpty(Mono.error(new TimeSlotBookingException(ErrorCode.TIME_SLOT_NOT_AVAILABLE)));
    }
}
