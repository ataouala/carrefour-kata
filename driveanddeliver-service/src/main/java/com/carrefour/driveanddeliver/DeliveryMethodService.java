package com.carrefour.driveanddeliver;

import reactor.core.publisher.Flux;

public interface DeliveryMethodService {
    Flux<DeliveryMethodDTO> getAllDeliveryMethods();
}
