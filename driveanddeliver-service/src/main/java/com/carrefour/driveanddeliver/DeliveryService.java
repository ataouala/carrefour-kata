package com.carrefour.driveanddeliver;

import reactor.core.publisher.Mono;

public interface DeliveryService {

    Mono<DeliveryDTO> createDelivery(CreateDeliveryDTO createDeliveryDTO);
}
