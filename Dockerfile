# Use a slim Java 21 base image
FROM eclipse-temurin:21-jdk-alpine

# Optional: Define an argument for the JAR file location
ARG JAR_FILE=hubee-supplier-controller/target/*-exec.jar

# Copy the JAR file from the build context to the container
COPY ${JAR_FILE} app.jar

# Define a working directory for the application
WORKDIR /app

# Expose the port where the application runs (replace 8080 with your actual port)
EXPOSE 8080

# Set the entrypoint to run the JAR file
ENTRYPOINT ["java", "-jar", "/app.jar"]
