package com.carrefour.driveanddeliver;

import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Repository
public interface TimeSlotRepository extends ReactiveCrudRepository<TimeSlot, Long> {

    Flux<TimeSlot> findByDeliveryMethodIdAndAvailableSlotsGreaterThan(String deliveryMethodId, int availableSlots);

    Mono<TimeSlot> findByIdAndDeliveryMethodId(Long id, String deliveryMethod);

}

