package com.carrefour.driveanddeliver;

import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DeliveryMethodRepository extends ReactiveCrudRepository<DeliveryMethod, String> {

}
