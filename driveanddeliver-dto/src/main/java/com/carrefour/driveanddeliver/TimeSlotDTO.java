package com.carrefour.driveanddeliver;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalTime;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class TimeSlotDTO {
    private Long id;
    private String deliveryMethodId;
    private LocalTime startTime;
    private LocalTime endTime;
}

