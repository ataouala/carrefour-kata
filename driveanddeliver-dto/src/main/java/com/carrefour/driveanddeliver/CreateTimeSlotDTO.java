package com.carrefour.driveanddeliver;

import lombok.Data;

import java.time.LocalTime;

@Data
public class CreateTimeSlotDTO {
    private DeliveryMethod deliveryMethod;
    private LocalTime startTime;
    private LocalTime endTime;
}
