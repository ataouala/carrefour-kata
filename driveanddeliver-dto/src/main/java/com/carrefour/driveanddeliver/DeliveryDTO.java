package com.carrefour.driveanddeliver;


import lombok.Data;

import java.time.LocalDate;

@Data
public class DeliveryDTO {
    private String deliveryMethodId;
    private LocalDate deliveryDate;
}
