package com.carrefour.driveanddeliver;


import lombok.Data;

import java.time.LocalDate;

@Data
public class CreateDeliveryDTO {
    private String deliveryMethodId;
    private LocalDate deliveryDate;
    private Long timeSlotId;
}

