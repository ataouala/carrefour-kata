package com.carrefour.driveanddeliver;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class DeliveryMethodDTO {
    private String id;
    private String method;
}
