-- Drop tables if they exist
DROP TABLE IF EXISTS deliveries;
DROP TABLE IF EXISTS time_slots;
DROP TABLE IF EXISTS delivery_methods;
DROP TABLE IF EXISTS users;

-- Create delivery_methods table
CREATE TABLE delivery_methods (
                                  id VARCHAR(36) PRIMARY KEY,
                                  method VARCHAR(50) NOT NULL
);

CREATE TABLE users (
                       id SERIAL PRIMARY KEY,
                       first_name VARCHAR(255),
                       last_name VARCHAR(255)
);
-- Create deliveries table
CREATE TABLE deliveries (
                            id SERIAL PRIMARY KEY,
                            delivery_method_id VARCHAR(36) REFERENCES delivery_methods(id),
                            delivery_date DATE,
                            time_slot_id BIGINT,
                            user_id INTEGER REFERENCES users(id)
);

-- Create time_slots table
CREATE TABLE time_slots (
                            id SERIAL PRIMARY KEY,
                            delivery_method_id VARCHAR(36) REFERENCES delivery_methods(id),
                            start_time TIME,
                            end_time TIME,
                            available_slots INT
);



-- Insert delivery methods
INSERT INTO delivery_methods (id, method) VALUES
                                              ('DRIVE', 'DRIVE'),
                                              ('DELIVERY', 'DELIVERY'),
                                              ('DELIVERY_TODAY', 'DELIVERY_TODAY'),
                                              ('DELIVERY_ASAP', 'DELIVERY_ASAP');

-- Insert test data into deliveries table
INSERT INTO deliveries (delivery_method_id, delivery_date, time_slot_id) VALUES
                                                                             ('DRIVE', '2024-06-17', 1),
                                                                             ('DELIVERY', '2024-06-17', 2),
                                                                             ('DELIVERY_TODAY', '2024-06-17', 3),
                                                                             ('DELIVERY_ASAP', '2024-06-17', 4);

-- Insert test data into time_slots table
INSERT INTO time_slots (delivery_method_id, start_time, end_time, available_slots) VALUES
                                                                                       ('DRIVE', '09:00:00', '10:00:00', 5),
                                                                                       ('DELIVERY', '11:00:00', '12:00:00', 3),
                                                                                       ('DELIVERY_TODAY', '13:00:00', '14:00:00', 4);



